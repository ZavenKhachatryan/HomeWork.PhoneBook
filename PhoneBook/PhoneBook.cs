﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork.PhoneBook
{
    class PhoneBook : Dictionary<string, Contact>
    {
        public Contact this[int index]
        {
            get { return OnGet(index); }
            set
            {
                Contact c = OnGet(index);
                if (!ContainsKey(c.Phone) && c != null)
                {
                    c.Phone = value.Phone;
                    this[value.Phone] = value;
                }

            }
        }
        private Contact OnGet(int index)
        {
            if (index < 0 || index > Count)
                return null;

            int i = 0;
            foreach (Contact item in Values)
            {
                if (i == index)
                    return item;
                i++;
            }
            return null;
        }
        public void Add(Contact item)
        {
            if (!ContainsKey(item.Phone))
                base.Add(item.Phone, item);
        }
        public void AddRange(List<Contact> contactList)
        {
            foreach (Contact item in contactList)
                Add(item);
        }
        public List<Contact> Find(string phoneCode)
        {
            string code = phoneCode;
            if (code.StartsWith("0"))
                code = code.Substring(1);

            code = $"+374 {code}";

            var items = new List<Contact>();
            foreach (Contact item in Values)
            {
                if (item.Phone.StartsWith(code))
                    items.Add(item);
            }

            return items;
        }
        public void Replace(Contact item)
        {
            if (ContainsKey(item.Phone))
                this[item.Phone] = item;
        }
    }
}