﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork.PhoneBook
{
    class Contact
    {
        private string phone;
        public string Phone
        {
            get { return phone; }
            set
            {
                if (value.StartsWith("+374") || value.StartsWith("00374"))
                    phone = value;
            }
        }

        public string Name { get; set; }
        public string Surname { get; set; }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                if (value.EndsWith("@gmail.com") || value.EndsWith("@mail.ru") || value.EndsWith("@yahoo.com"))
                    email = value;
            }
        }

        public string FullInformation => $"{Phone} , {Name} , {Surname} , {Email}";

        public override string ToString()
        {
            return FullInformation;
        }
    }
}