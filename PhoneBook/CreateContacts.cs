﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork.PhoneBook
{
    class CreateContacts : PhoneBook
    {
        public static List<Contact> CreateContact(int count)
        {
            var codes = new string[] { "77", "41", "43", "55", "91", "93", "94", "95", "96", "97", "98", "99", };
            var emails = new string[] { "@mail.ru", "@gmail.com", "@yahoo.com" };
            var rnd = new Random();

            var contacts = new List<Contact>(count);
            for (int i = 0; i < count; i++)
            {
                int index = rnd.Next(0, codes.Length);
                int num = rnd.Next(10, 99);
                var contact = new Contact
                {
                    Phone = $"+374 {codes[index]} {num} {num} {num}",
                    Name = $"A{i + 1}",
                    Surname = $"A{i + 1}yan",
                    Email = $"A{i + 1}{emails[i % emails.Length]}"
                };
                contacts.Add(contact);
            }
            return contacts;
        }
    }
}
