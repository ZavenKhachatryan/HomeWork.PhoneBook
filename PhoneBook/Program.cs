﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork.PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var contactList = CreateContacts.CreateContact(20);

            var phoneBook = new PhoneBook();
            phoneBook.AddRange(contactList);

            var c = new Contact
            {
                Phone = "+374 TestPhoneNumber",
            };

            phoneBook[4] = c;
            phoneBook[c.Phone] = c;

            var c1 = new Contact
            {
                Phone = "+374 TestPhoneNumber",
                Name = "Test",
                Surname = "Testyan",
                Email = "Test@gmail.com"
            };

            phoneBook.Replace(c1);

            List<Contact> find = phoneBook.Find("093");

            foreach (var item in find)
                Console.WriteLine(item);

            Console.ReadLine();
        }
    }
}